<?php
/**
 * Copyright (c) Wyzen 2018
 */

/**
 * Class de test retournant true / false
 * @author vdeapps
 * @version
 */

namespace Wyzen\phpCore;

/**
 * @category awc
 *
 * @uses     awcRegistry
 *
 */
class Helper
{
    /**
     * Retourne une chaine en camelCase avec lcfirst|ucfirst
     *
     * @param string $str
     * @param string $firstChar lcfirst|ucfirst default(lcfirst)
     *
     * @return string
     */
    public static function camelCase($str, $firstChar = 'lcfirst')
    {
        $str = str_replace(['-', '_', '.'], ' ', $str);
        $str = mb_convert_case($str, MB_CASE_TITLE);
        $str = str_replace(' ', '', $str); //ucwords('ghjkiol|ghjklo', "|");
        
        if (!function_exists($firstChar)) {
            $firstChar = 'lcfirst';
        }
        $str = call_user_func($firstChar, $str);
        
        return $str;
    }
    
    /**
     * base64_decode sans les == de fin
     *
     * @param      $str
     * @param bool $stripEgal
     *
     * @return string
     */
    public static function base64Encode($str, $stripEgal = true)
    {
        $str64 = base64_encode($str);
        if ($stripEgal) {
            return rtrim(strtr($str64, '+/', '-_'), '=');
        }
        
        return $str64;
    }
    
    /**
     * base64_decode
     *
     * @param $str
     *
     * @return bool|string
     */
    public static function base64Decode($str)
    {
        return base64_decode(str_pad(strtr($str, '-_', '+/'), strlen($str) % 4, '=', STR_PAD_RIGHT));
    }
    
    /**
     * Compare 2 tableaux de valeurs ou 2 valeurs
     *
     * @param mixed|array $arr1      La variable sera transformée en tableau
     * @param mixed|array $arr2      La variable sera transformée en tableau
     * @param string      $operateur default(IN), NOTIN, EQ
     *
     * @return boolean
     */
    public static function compareValues($arr1 = [], $arr2 = [], $operateur = 'IN')
    {
        if (!is_array($arr1)) {
            $arr1 = [$arr1];
        }
        if (!is_array($arr2)) {
            $arr2 = [$arr2];
        }
        
        if (!is_array($arr1) || !is_array($arr2)) {
            return false;
        }
        
        switch (strtoupper($operateur)) {
            case 'IN':
                $result = (count(array_intersect($arr1, $arr2)) !== 0);
                break;
            
            case 'NOTIN':
                $result = (count(array_diff($arr1, $arr2)) !== 0);
                break;
            
            case 'EQ':
                $result = (count(array_intersect($arr1, $arr2)) == count($arr2));
                break;
            
            default:
                $result = false;
                break;
        }
        
        return $result;
    }
    
    /**
     * Retourne le tableau de résulats par une autre clé
     *
     * @param array  $array
     * @param string $newKey
     *
     * @param bool   $append add new Row if multiple same key
     *
     * @return array
     */
    public static function indexedBy(&$array, $newKey, $append = false)
    {
        $result = [];
        $nbRows = count($array);
        
        foreach ($array as $row) {
            $valOfKey = $row[$newKey];
            
            if ($append === false) {
                $result[$valOfKey] = $row;
            }
            else {
                $result[$valOfKey][] = $row;
            }
        }
        
        return $result;
    }
}

